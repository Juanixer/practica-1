/*
Copyright [2022] [Juan García-Obregón]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,2
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
either express or implied. See the License for the specific
language governing permissions and limitations under the
License.

*/


package mates;


public class Matematicas{



/**
* Genera una aproximación al número pi mediante el método de
* Montecarlo. El parámetro `pasos` indica el número de puntos
* generado.
*/   
  

	
	/**
	 * @author Juan García-Obregón
	 * @version java 11.0.12
	 * 
	 * @see a traves del metodo montecarlo se genera una aproximación a pi y se
	 * crea un parametro que establecerá el numero de puntos/lanzamientos generado.
	*/



     public static double generarNumeroPiIterativo(long pasos){	        
double a = 0;

	for(long i = 0; i < pasos; i++){

	double x = Math.random();
		double y = Math.random();


		if ((x*x) + (y*y) <= 1){
		a++;}
	}
	double pi = 4*a/pasos;
	return pi; 
     }

	
     }




}

